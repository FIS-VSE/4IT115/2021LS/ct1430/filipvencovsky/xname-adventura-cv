package cz.vse.xname.x1430AdventuraCv.main;

public interface PredmetPozorovani {
    /**
     * metoda pro přihlášení nového pozorovatele
     * @param pozorovatel
     */
    void registruj(Pozorovatel pozorovatel);

    /**
     * metoda pro odhlášení pozorovatele
     * @param pozorovatel
     */
    void odRegistruj(Pozorovatel pozorovatel);

    /**
     * metoda upozorni pozorovatele může být privátní
     */
    // void uzpozorniPozorovatele()

}
