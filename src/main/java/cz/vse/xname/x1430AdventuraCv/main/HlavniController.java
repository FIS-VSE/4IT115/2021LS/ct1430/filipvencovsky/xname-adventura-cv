package cz.vse.xname.x1430AdventuraCv.main;

import cz.vse.xname.x1430AdventuraCv.logika.Hra;
import cz.vse.xname.x1430AdventuraCv.logika.IHra;
import cz.vse.xname.x1430AdventuraCv.logika.Prostor;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.util.HashMap;
import java.util.Map;

public class HlavniController implements Pozorovatel {

    @FXML private ImageView hrac;
    @FXML private Button odesli;
    @FXML private TextArea vystup;
    @FXML private TextField vstup;
    @FXML private ListView seznamVychodu;
    Map<String, Point2D> poziceProstoru = new HashMap<>();

    private IHra hra;

    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });

        vystup.setEditable(false);

        hra = new Hra();
        hra.getHerniPlan().registruj(this);
        vystup.appendText(hra.vratUvitani()+"\n\n");

        poziceProstoru.put("domeček", new Point2D(20,76));
        poziceProstoru.put("les", new Point2D(87,45));
        poziceProstoru.put("hluboký_les", new Point2D(151,83));
        poziceProstoru.put("chaloupka", new Point2D(224,38));
        poziceProstoru.put("jeskyně", new Point2D(158,159));

        seznamVychodu.getItems().addAll(hra.getHerniPlan().getAktualniProstor().getVychody());
    }

    public void odesliVstup(ActionEvent actionEvent) {
        String textVstupu = vstup.getText();
        zpracujPrikaz(textVstupu);
        vstup.clear();
    }

    private void zpracujPrikaz(String prikaz) {

        String vysledek = hra.zpracujPrikaz(prikaz);

        vystup.appendText("Příkaz: "+prikaz+"\n");
        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog()+"\n\n");
            vstup.setDisable(true);
            odesli.setDisable(true);
            seznamVychodu.setDisable(true);
        }
    }

    @Override
    public void update() {
        seznamVychodu.getItems().clear();
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        seznamVychodu.getItems().addAll(aktualniProstor.getVychody());
        hrac.setLayoutX(poziceProstoru.get(aktualniProstor.getNazev()).getX());
        hrac.setLayoutY(poziceProstoru.get(aktualniProstor.getNazev()).getY());
    }

    public void kliknutniSeznamVychodu(MouseEvent mouseEvent) {
        Prostor vybranyProstor = (Prostor) seznamVychodu.getSelectionModel().getSelectedItem();
        zpracujPrikaz("jdi "+vybranyProstor);
    }
}
