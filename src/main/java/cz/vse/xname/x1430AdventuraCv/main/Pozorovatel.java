package cz.vse.xname.x1430AdventuraCv.main;

public interface Pozorovatel {
    /**
     * Metoda, kterou provede pozorovatel při změně v pozorovaném předmětu
     */
    void update();
}
